#include <libunittest/all.hpp>

namespace ut  = unittest;
namespace uta = ut::assertions;

#include <imperium/cards/card_database.hpp>
namespace ic = imperium::cards;

// a test class without a test context
struct test_card_database : ut::testcase<> 
{

    // this static method runs the tests
    static void run()
    {
        UNITTEST_CLASS(test_card_database);
        UNITTEST_RUN(test_init);
        // UNITTEST_RUN(copy_and_assignent);
        // UNITTEST_RUN(append_and_push_back);
        // UNITTEST_RUN_TIME(test_in_container, 2) // a timeout of 2s
        // UNITTEST_RUN_SKIP(test_that_is_skipped, "for demo purposes")
        // UNITTEST_RUN_MAYBE(test_that_is_run, true, "message for when not run")
    }

    test_card_database() {} // executed before each test and before set_up()
    ~test_card_database() {} // executed after each test and after tear_down()

    void set_up() {} // executed before each test and after constructor
    void tear_down() {} // executed after each test and before destructor

    void test_init()
    {
      ic::card_database db;
      db.init();
      uta::assert_equal(22, db.bypassed_cards().size());
    }

};

REGISTER(test_card_database) // this registers the test class