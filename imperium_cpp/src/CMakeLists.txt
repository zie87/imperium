PROJECT(Imperium)

################################################################################
# IMPERIUM CODE CODE
################################################################################
ADD_DEFINITIONS(-DAPP_DATA_DIR="${CMAKE_CURRENT_SOURCE_DIR}/imperium/ressources")

ADD_LIBRARY(imperium SHARED  "${CMAKE_CURRENT_SOURCE_DIR}/imperium/cards/base_card.cpp"
                             "${CMAKE_CURRENT_SOURCE_DIR}/imperium/cards/card_database.cpp"
                             
                             "${CMAKE_CURRENT_SOURCE_DIR}/imperium/cards/card_effect_interface.cpp"
                             "${CMAKE_CURRENT_SOURCE_DIR}/imperium/cards/effects/card_effects_base.cpp"

                             "${CMAKE_CURRENT_SOURCE_DIR}/imperium/game/game_data.cpp"
                             "${CMAKE_CURRENT_SOURCE_DIR}/imperium/game/decisions.cpp"
                             "${CMAKE_CURRENT_SOURCE_DIR}/imperium/game/state.cpp"
                             "${CMAKE_CURRENT_SOURCE_DIR}/imperium/game/player_state.cpp"

                             "${CMAKE_CURRENT_SOURCE_DIR}/imperium/game/event_interface.cpp"
                             "${CMAKE_CURRENT_SOURCE_DIR}/imperium/game/events/events.cpp"
                             "${CMAKE_CURRENT_SOURCE_DIR}/imperium/game/events/events_base.cpp")

# ADD_SUBDIRECTORY (plistcpp)