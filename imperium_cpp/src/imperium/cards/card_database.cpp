#include <imperium/cards/card_database.hpp>
#include <imperium/cards/effects/card_effects_base.hpp>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <fstream>
#include <sstream>
#include <iostream>


namespace imperium
{
namespace cards
{

  // util functions
  void fill_skip_list(boost::property_tree::ptree const& pt, typename card_database::name_list& vec )
  {
      using boost::property_tree::ptree;
      ptree::const_iterator end = pt.end();
      for (ptree::const_iterator it = pt.begin(); it != end; ++it) 
      {
        auto card_name =  it->second.get_value<std::string>();
        if(!card_name.empty())
        {
          vec.push_back(card_name);
        }
        fill_skip_list(it->second, vec);
      }
  }

  void read_skip_list(const std::string& filepath, typename card_database::name_list& vec )
  {
    boost::property_tree::ptree pt;
    boost::property_tree::read_json(filepath, pt);
    fill_skip_list(pt, vec);
  }

  std::string get_res_path(const std::string& filename)
  {
    std::string res_path = APP_DATA_DIR;
    res_path += '/'; res_path += filename;
    return res_path;
  }

  void load_card_properties(boost::property_tree::ptree const& pt, base_card_properties& properties )
  {

      properties.cost     = pt.get<unsigned short>("cost");
      properties.pcost    = pt.get<unsigned short>("pcost");
      properties.supply   = pt.get<unsigned short>("supply");  
      properties.actions  = pt.get<unsigned short>("actions");
      properties.cards    = pt.get<unsigned short>("cards"); 
      properties.buys     = pt.get<unsigned short>("buys"); 
      properties.money    = pt.get<unsigned short>("money");
      properties.v_tokens = pt.get<unsigned short>("v tokens");
    
      properties.v_points = pt.get<short>("v points");
      properties.treasure = pt.get<short>("treasure");

      auto types_child = pt.get_child("type");
      for(auto it : types_child)
      {
        properties.types.push_back(it.second.get_value<std::string>());
      }
  }

  //card_database
  card_database::card_database()
  : m_skip_list(), m_card_list(), m_cards()
  {}

  card_database::~card_database()
  {
    m_skip_list.clear();
    m_card_list.clear();
    m_cards.clear();
  }

  void card_database::init(bool use_custom_cards)
  {
    read_skip_list( get_res_path("skip_list.json"), m_skip_list );
    load_expansion_properties( get_res_path("core.json"), "core");
    load_expansion_properties( get_res_path("base.json"), "base");

    register_card_effects(use_custom_cards);
  }

  typename card_database::card_ptr card_database::get_card(const std::string& name) const
  {
    const auto it = m_cards.find(name);
    if(it == m_cards.end()) return nullptr;
    else return it->second;
  }

  void card_database::register_card_effects(bool use_custom_cards)
  {
    // Base cards
    register_cards("chapel",        effect_ptr(new effects::base_chapel));
    register_cards("cellar",        effect_ptr(new effects::base_cellar));
    register_cards("moneylender",   effect_ptr(new effects::base_moneylender));
    register_cards("workshop",      effect_ptr(new effects::base_workshop));
    register_cards("feast",         effect_ptr(new effects::base_feast));
    register_cards("chancellor",    effect_ptr(new effects::base_chancellor));
    register_cards("militia",       effect_ptr(new effects::base_militia));
    register_cards("council room",  effect_ptr(new effects::base_council_room));
    register_cards("remodel",       effect_ptr(new effects::base_remodel));
    
  //     RegisterCard("library", new CardLibrary);
  //     RegisterCard("witch", new CardWitch);
  //     RegisterCard("mine", new CardMine);
  //     RegisterCard("gardens", new CardGardens);
  //     RegisterCard("adventurer", new CardAdventurer);
  //     RegisterCard("bureaucrat", new CardBureaucrat);
  //     RegisterCard("spy", new CardSpy);
  //     RegisterCard("throne room", new CardThroneRoom);

  //     //
  //     // Intrigue cards
  //     //
  //     RegisterCard("pawn", new CardPawn);
  //     RegisterCard("shanty town", new CardShantyTown);
  //     RegisterCard("courtyard", new CardCourtyard);
  //     RegisterCard("steward", new CardSteward);
  //     RegisterCard("wishing well", new CardWishingWell);
  //     RegisterCard("baron", new CardBaron);
  //     RegisterCard("nobles", new CardNobles);
  //     RegisterCard("trading post", new CardTradingPost);
  //     RegisterCard("scout", new CardScout);
  //     RegisterCard("duke", new CardDuke);
  //     RegisterCard("conspirator", new CardConspirator);
  //     RegisterCard("ironworks", new CardIronworks);
  //     RegisterCard("swindler", new CardSwindler);
  //     RegisterCard("minion", new CardMinion);
  //     RegisterCard("saboteur", new CardSaboteur);
  //     RegisterCard("torturer", new CardTorturer);
  //     RegisterCard("upgrade", new CardUpgrade);
  //     RegisterCard("mining village", new CardMiningVillage);
  //     RegisterCard("tribute", new CardTribute);

  //     //
  //     // Seaside cards
  //     //
  //     RegisterCard("caravan", new CardCaravan);
  //     RegisterCard("fishing village", new CardFishingVillage);
  //     RegisterCard("merchant ship", new CardMerchantShip);
  //     RegisterCard("wharf", new CardWharf);
  //     RegisterCard("tactician", new CardTactician);
  //     RegisterCard("lighthouse", new CardLighthouse);
  //     RegisterCard("warehouse", new CardWarehouse);
  //     RegisterCard("salvager", new CardSalvager);
  //     RegisterCard("lookout", new CardLookout);
  //     RegisterCard("pearl diver", new CardPearlDiver);
  //     RegisterCard("navigator", new CardNavigator);
  //     RegisterCard("ambassador", new CardAmbassador);
  //     RegisterCard("sea hag", new CardSeaHag);
  //     RegisterCard("cutpurse", new CardCutpurse);
  //     RegisterCard("ghost ship", new CardGhostShip);
  //     RegisterCard("treasure map", new CardTreasureMap);
  //     RegisterCard("smugglers", new CardSmugglers);
  //     RegisterCard("island", new CardIsland);
  //     RegisterCard("explorer", new CardExplorer);

  //     //
  //     // Prosperity cards
  //     //
  //     RegisterCard("king's court", new CardKingsCourt);
  //     RegisterCard("rabble", new CardRabble);
  //     RegisterCard("mountebank", new CardMountebank);
  //     RegisterCard("goons", new CardGoons);
  //     RegisterCard("city", new CardCity);
  //     RegisterCard("counting house", new CardCountingHouse);
  //     RegisterCard("loan", new CardLoan);
  //     RegisterCard("venture", new CardVenture);
  //     RegisterCard("bishop", new CardBishop);
  //     RegisterCard("watchtower", new CardWatchtower);
  //     RegisterCard("expand", new CardExpand);
  //     RegisterCard("trade route", new CardTradeRoute);
  //     RegisterCard("mint", new CardMint);
  //     RegisterCard("vault", new CardVault);

      //
      // Custom cards
      //
    if(use_custom_cards)
    {
  //         RegisterCard("architect", new CardArchitect);
  //         RegisterCard("acolyte", new CardAcolyte);
  //         RegisterCard("aqueduct", new CardAqueduct);
  //         RegisterCard("gambler", new CardGambler);
  //         RegisterCard("meadow", new CardMeadow);
  //         RegisterCard("ruins", new CardRuins);
  //         RegisterCard("squire", new CardSquire);
  //         RegisterCard("haunted village", new CardHauntedVillage);
  //         RegisterCard("betrayers", new CardBetrayers);
  //         RegisterCard("meadow", new CardMeadow);
  //         RegisterCard("ruins", new CardRuins);
  //         RegisterCard("aristocrat", new CardAristocrat);
  //         RegisterCard("cursed land", new CardCursedLand);
  //         RegisterCard("promised land", new CardPromisedLand);
  //         RegisterCard("palace", new CardPalace);
  //         RegisterCard("pauper", new CardPauper);
  //         RegisterCard("floating city", new CardFloatingCity);
  //         RegisterCard("grand court", new CardGrandCourt);
  //         RegisterCard("knight", new CardKnight);
  //         RegisterCard("street urchin", new CardStreetUrchin);
  //         RegisterCard("sepulcher", new CardSepulcher);
  //         RegisterCard("benefactor", new CardBenefactor);
  //         RegisterCard("champion", new CardChampion);
  //         RegisterCard("hex", new CardHex);
  //         RegisterCard("trailblazer", new CardTrailblazer);
  //         RegisterCard("wager", new CardWager);
  //         RegisterCard("witchdoctor", new CardWitchdoctor);
  //         RegisterCard("pillage", new CardPillage);
  //         RegisterCard("furnace", new CardFurnace);
  //         RegisterCard("evangelist", new CardEvangelist);
    }
  }

  void card_database::register_cards(const std::string& card_name, effect_ptr effect)
  {
      auto card = m_cards[card_name];
      effect->card = card;
      card->effect(effect);
  }

  void card_database::load_expansion_properties(const std::string& filepath, const std::string& expansion)
  {
    boost::property_tree::ptree pt;
    boost::property_tree::read_json(filepath, pt);

    for (auto it = pt.begin(); it != pt.end(); ++it) 
    {
      base_card_properties props(it->first, expansion);
      load_card_properties( it->second, props );
      m_cards.insert( card_map_pair( props.name, std::make_shared<base_card>(props) ) );
    }
  }

} // cards
} // imperium


// #include "Main.h"

// void CardDatabase::Init(bool useCustomCards)
// {
//     _skipList = Utility::GetFileLines("data/skipList.txt");

//     LoadCardList("data/core.txt", "core");
//     LoadCardList("data/base.txt", "base");
//     LoadCardList("data/intrigue.txt", "intrigue");
//     LoadCardList("data/seaside.txt", "seaside");
//     LoadCardList("data/alchemy.txt", "alchemy");
//     LoadCardList("data/prosperity.txt", "prosperity");
//     if(useCustomCards) LoadCardList("data/custom.txt", "custom");

//     LoadPriorityList();
//     LoadStrengthList();

//     RegisterCardEffects(useCustomCards);
// }



// void CardDatabase::LoadPriorityList()
// {
//     Vector<String> lines = Utility::GetFileLines("data/priority.txt", 2);
//     for(UINT lineIndex = 0; lineIndex < lines.Length(); lineIndex++)
//     {
//         const String &s = lines[lineIndex];
//         if(!_skipList.Contains(s))
//         {
//             //PersistentAssert(_cards.find(s) != _cards.end(), "Card not in database");
//             if(_cards.find(s) != _cards.end()) _cards[s]->priority = int(lines.Length()) - int(lineIndex);
//         }
//     }
// }

// void CardDatabase::LoadStrengthList()
// {
//     Vector<String> lines = Utility::GetFileLines("data/strongCards.txt", 2);
//     for(UINT lineIndex = 0; lineIndex < lines.Length(); lineIndex++)
//     {
//         const String &s = lines[lineIndex];
//         if(!_skipList.Contains(s))
//         {
//             //PersistentAssert(_cards.find(s) != _cards.end(), "Card not in database");
//             if(_cards.find(s) != _cards.end()) _cards[s]->strength = int(lines.Length()) - int(lineIndex);
//         }
//     }
// }





// bool CardDatabase::IsSupplyCard(const Card *c) const
// {
//     return (c->expansion != "core");
// }

// Card* CardDatabase::RandomSupplyCard() const
// {
//     Card *result;
//     do
//     {
//         result = _cardList.RandomElement();
//     } while(!IsSupplyCard(result));
//     return result;
// }
