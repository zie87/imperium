
#include <imperium/cards/card_effect_interface.hpp>

#include <imperium/game/state.hpp>
#include <imperium/game/decisions.hpp>

namespace imperium
{
namespace cards
{
  card_effect_interface::~card_effect_interface() {}

  void card_effect_interface::play_action(game::state&) const {}
  bool card_effect_interface::can_process_decisions() const { return false; }

  void card_effect_interface::process_decision(game::state&, const game::decision_response&) const { throw "Card does not support decisions"; }
      
  void card_effect_interface::process_duration(game::state& ) const {}
  unsigned short card_effect_interface::victory_points(const game::state& , unsigned int ) const { return 0; }


} // cards
} // imperium