#include <imperium/cards/base_card.hpp>

namespace imperium
{
namespace cards
{
  std::ostream& operator<<(std::ostream& os, const base_card_properties& p)
  {
    os << std::endl
       << "name: " << p.name << " expansion: " << p.expansion << std::endl
       << "cost: "  << p.cost << " pcost: " << p.pcost << " supply: " << p.supply << std::endl
       << "types: ";
        for ( auto it : p.types ) os << it << " ";
    os << std::endl
       << "actions: " << p.actions << " cards: " << p.cards << " buys: " << p.buys << " money: " << p.money << std::endl
       << "v_tokens: " << p.v_tokens << " v_points: " << p.v_points << " treasure: " << p.treasure << std::endl;

    return os;
  }

  base_card::base_card() {}
  base_card::base_card(const base_card_properties& props) { set_properties(props); }

  base_card::~base_card() {}

  void base_card::set_properties(const base_card_properties& properties) noexcept
  {

      // general
      name       = properties.name;
      expansion  = properties.expansion;

      costs          = properties.cost;
      portion_costs  = properties.pcost;
      supply         = properties.supply;

      // actions
      actions  = properties.actions;
      cards    = properties.cards;
      buys     = properties.buys;
      money    = properties.money;
      
      // victory and treasur
      victory_tokens = properties.v_tokens;
      victory_points = properties.v_points;
      treasure       = properties.treasure;
  
      // types
      for(auto it : properties.types)
      {
             if( it == "victory" )   { m_is_victory   = true; }
        else if( it == "treasure" )  { m_is_treasure  = true; }
        else if( it == "action" )    { m_is_action    = true; }
        else if( it == "attack" )    { m_is_attack    = true; }
        else if( it == "reaction" )  { m_is_reaction  = true; }
        else if( it == "duration" )  { m_is_duration  = true; }
        else if( it == "permanent" ) { m_is_permanent = true; }
      }
  }

  bool base_card::is_victory()   const noexcept { return m_is_victory; }
  bool base_card::is_treasure()  const noexcept { return m_is_treasure; }
  bool base_card::is_action()    const noexcept { return m_is_action; }
  bool base_card::is_reaction()  const noexcept { return m_is_reaction; }
  bool base_card::is_duration()  const noexcept { return m_is_duration; }
  bool base_card::is_attack()    const noexcept { return m_is_attack; }
  bool base_card::is_permanent() const noexcept { return m_is_permanent; }

  void base_card::set_victory(bool is)   noexcept { m_is_victory   = is; }
  void base_card::set_treasure(bool is)  noexcept { m_is_treasure  = is; }
  void base_card::set_action(bool is)    noexcept { m_is_action    = is; }
  void base_card::set_reaction(bool is)  noexcept { m_is_reaction  = is; }
  void base_card::set_duration(bool is)  noexcept { m_is_duration  = is; }
  void base_card::set_attack(bool is)    noexcept { m_is_attack    = is; }
  void base_card::set_permanent(bool is) noexcept { m_is_permanent = is; }

  bool base_card::is_pure_victory() const noexcept { return (m_is_victory && !m_is_treasure && !m_is_action && !m_is_duration);  }

  typename base_card::effect_ptr base_card::effect() const noexcept { return m_card_effect; }
  void base_card::effect(card_effect_interface* e) noexcept {  m_card_effect.reset(e); }
  void base_card::effect(effect_ptr e) noexcept { m_card_effect = e; };
} // cards
} // imperium