
#include <imperium/cards/effects/card_effects_base.hpp>

#include <imperium/game/events/events.hpp>
#include <imperium/game/events/events_base.hpp>

#include <imperium/game/state.hpp>
#include <imperium/game/decisions.hpp>

#include <iostream>
#include <memory>

namespace ige = imperium::game::events;

namespace imperium
{
namespace cards
{
namespace effects
{

  //////////////////////////////////////////////////////////////////////////////
  // CHAPEL
  //////////////////////////////////////////////////////////////////////////////
  void base_chapel::play_action(game::state& s) const 
  {
    // could overrun players vector size!
    if( !s.players.at(s.player).hand.empty() )
    {
      s.decision.select_cards(card, 0, 4);
      s.decision.text = "[base_cellar] Select up to 4 cards to trash:";
      s.decision.card_choices = s.players[s.player].hand;
    } else
    {
      // TODO: needs change to real log!
      std::cout << "[base_chapel] player " << s.player << " has no cards to trash" << std::endl;
    }
  }
  
  bool base_chapel::can_process_decisions() const { return true; }

  void base_chapel::process_decision(game::state& s, const game::decision_response& response) const 
  {
    for( auto c : response.cards )
    {
      s.stack.push_back( std::shared_ptr<game::event_interface>(new ige::trash_card_from_hand(s.player, card)) );
    }
  }

  //////////////////////////////////////////////////////////////////////////////
  // CELLAR
  //////////////////////////////////////////////////////////////////////////////

  void base_cellar::play_action(game::state& s) const
  {
    if( !s.players.at(s.player).hand.empty() )
    {
      s.decision.select_cards(card, 0, s.players[s.player].hand.size());
      s.decision.text = "[base_cellar] Select cards to discard:";
      s.decision.card_choices = s.players[s.player].hand;
    }
    else
    {
      std::cout << "[base_cellar] player " << s.player << " has no cards to discard" << std::endl;
    }
  }
  bool base_cellar::can_process_decisions() const { return true; }
  void base_cellar::process_decision(game::state& s, const game::decision_response& response) const
  {
    for( auto it : response.cards) 
    { 
      s.stack.push_back( std::shared_ptr<game::event_interface>(new ige::draw_card(s.player)) ); 
    }
    for( auto c : response.cards)
    {
      s.stack.push_back( std::shared_ptr<game::event_interface>(new ige::discard_card(s.player, c, ige::discard_zone::hand) ) );
    }
  }

  //////////////////////////////////////////////////////////////////////////////
  // MONEYLENDER
  //////////////////////////////////////////////////////////////////////////////

  void base_moneylender::play_action(game::state& s) const
  {
    bool in_hand = false;
    for( auto it : s.players[s.player].hand ) if( it->name == "copper" ) in_hand = true;
    if( in_hand )
    {
      s.stack.push_back( std::shared_ptr<game::event_interface>(new ige::trash_card_from_hand(s.player, s.data->base_cards.copper)) );
      s.players[s.player].money += 3;
    }
    else
    {
      std::cout << "[base_moneylender] player " << s.player << "has no coppers to trash" << std::endl;
    }
  }

  //////////////////////////////////////////////////////////////////////////////
  // WORKSHOP
  //////////////////////////////////////////////////////////////////////////////

  void base_workshop::play_action(game::state& s) const
  {
    s.decision.gain_card_from_supply(s, card, 0, 4);
    if(s.decision.card_choices.empty())
    {
      s.decision.type = game::decision_type::none;
      // LOGGING
      std::cout << "[base_workshop] player " << s.player << " has no cards to gain" << std::endl;
    }
  }
  bool base_workshop::can_process_decisions() const { return true; }
  void base_workshop::process_decision(game::state& s, const game::decision_response& response) const
  {
    s.data->players.at(s.decision.controlling_player).ledger.record_buy(response.cards[0]);
    s.stack.push_back( std::shared_ptr<game::event_interface>( new ige::gain_card(s.player, response.cards[0])) );
  }

  //////////////////////////////////////////////////////////////////////////////
  // FEAST
  //////////////////////////////////////////////////////////////////////////////

  void base_feast::play_action(game::state& s) const
  {
    s.decision.gain_card_from_supply(s, card, 0, 4);
    if(s.decision.card_choices.empty())
    {
      s.decision.type = game::decision_type::none;
      // LOGGING
      std::cout << "[base_feast] player " << s.player << " has no cards to gain" << std::endl;
    }
  }
  bool base_feast::can_process_decisions() const { return true; }
  void base_feast::process_decision(game::state& s, const game::decision_response& response) const
  {
    s.data->players.at(s.decision.controlling_player).ledger.record_buy(response.cards[0]);
    s.stack.push_back( std::shared_ptr<game::event_interface>( new ige::trash_card_from_play(s.player, card)) );
    s.stack.push_back( std::shared_ptr<game::event_interface>( new ige::gain_card(s.player, response.cards[0])) );
  }

  //////////////////////////////////////////////////////////////////////////////
  // CHANCELLOR
  //////////////////////////////////////////////////////////////////////////////

  void base_chancellor::play_action(game::state& s) const
  {
    s.decision.make_discrete_choice(card, 2);
    s.decision.text = "Put your deck into your discard pile?|Yes|No";
  }
  bool base_chancellor::can_process_decisions() const { return true; }
  void base_chancellor::process_decision(game::state& s, const game::decision_response& response) const
  {
    if(response.choice == 0)
    {
      // LOGGING
      std::cout << "[base_chancellor] player " << s.player << " discards their deck" << std::endl;
      game::player_state& p = s.players[s.player];
      for( auto it : p.deck ) p.discard_pile.push_back(it);
      p.deck.clear();
    }
    else
    {
      // LOGGING
      std::cout << "[base_chancellor] player " << s.player << " does not discard their deck" << std::endl;
    }
  }
  //////////////////////////////////////////////////////////////////////////////
  // MILITIA
  //////////////////////////////////////////////////////////////////////////////
  void base_militia::play_action(game::state& s) const
  {
    for(const game::player_info& p : s.data->players)
    {
      if(p.index != s.player)
      {
        s.stack.push_back( std::shared_ptr<game::event_interface>(new ige::discard_down_to_n(card, p.index, 3)) );
      }
    }
  }
  //////////////////////////////////////////////////////////////////////////////
  // COUNCIL ROOM
  //////////////////////////////////////////////////////////////////////////////
  void base_council_room::play_action(game::state& s) const
  {
    for(const game::player_info& p : s.data->players)
    {
      if( p.index != s.player)
      {
        s.stack.push_back(std::shared_ptr<game::event_interface>(new ige::draw_card(p.index)) );
      }
    }
  }
  //////////////////////////////////////////////////////////////////////////////
  // REMODEL
  //////////////////////////////////////////////////////////////////////////////
  void base_remodel::play_action(game::state& s) const
  {
    if( !s.players[s.player].hand.empty() )
    {
      s.decision.select_cards(card, 1, 1);
      s.decision.text = "Select a card to trash:";
      s.decision.card_choices = s.players[s.player].hand;
      s.stack.push_back( std::shared_ptr<game::event_interface>(new ige::remodel_expand(card, 2)) );
    }
    else
    {
      // LOGGING
      std::cout << "[base_chancellor] player " << s.player << " has no cards to trash" << std::endl;
    }
  }

} // effects
} // cards
} // imperium

/*

class CardRemodel : public CardEffect
{
public:
    void PlayAction(State &s) const
    {

    }
};

class CardMine : public CardEffect
{
public:
    void PlayAction(State &s) const
    {
        if(s.players[s.player].TreasureCount() > 0)
        {
            s.decision.SelectCards(c, 1, 1);
            if(decisionText) s.decision.text = "Select a treasure to trash:";
            for(Card *c : s.players[s.player].hand)
            {
                if(c->isTreasure) s.decision.AddUniqueCard(c);
            }
            s.stack.PushEnd(new EventMine(c));
        }
        else
        {
            if(logging) s.Log("has no treasures to trash");
        }
    }
};

class CardLibrary : public CardEffect
{
public:
    void PlayAction(State &s) const
    {
        if(s.players[s.player].hand.Length() < 7)
        {
            s.stack.PushEnd(new EventLibrary(c));
        }
        else
        {
            if(logging) s.Log("already has 7 cards in hand");
        }
    }
};

class CardAdventurer : public CardEffect
{
public:
    void PlayAction(State &s) const
    {
        s.stack.PushEnd(new EventAdventurer(c));
    }
};

class CardWitch : public CardEffect
{
public:
    void PlayAction(State &s) const
    {
        for(const PlayerInfo &p : s.data->players)
        {
            if(p.index != s.player)
            {
                s.stack.PushEnd(new EventGainCard(p.index, s.data->baseCards.curse, false, true, GainToDiscard));
            }
        }
    }
};

class CardGardens : public CardEffect
{
public:
    int VictoryPoints(const State &s, UINT playerIndex) const
    {
        return s.players[playerIndex].TotalCards() / 10;
    }
};

class CardBureaucrat : public CardEffect
{
public:
    void PlayAction(State &s) const
    {
        s.stack.PushEnd(new EventGainCard(s.player, s.data->baseCards.silver, false, false, GainToDeckTop));
        for(const PlayerInfo &p : s.data->players)
        {
            if(p.index != s.player)
            {
                s.stack.PushEnd(new EventBureaucratAttack(c, p.index));
            }
        }
    }
    bool CanProcessDecisions() const
    {
        return true;
    }
    void ProcessDecision(State &s, const DecisionResponse &response) const
    {
        Card *c = response.cards[0];
        PlayerState &p = s.players[s.decision.controllingPlayer];
        p.hand.RemoveSwap(p.hand.FindFirstIndex(c));
        p.deck.PushEnd(c);
        if(logging) s.LogIndent(1, s.decision.controllingPlayer, "puts " + c->PrettyName() + " on their deck");
    }
};

class CardSpy : public CardEffect
{
public:
    void PlayAction(State &s) const
    {
        for(const PlayerInfo &p : s.data->players)
        {
            s.stack.PushEnd(new EventSpy(c, s.player, p.index));
        }
    }
};

class CardThroneRoom : public CardEffect
{
public:
    void PlayAction(State &s) const
    {
        s.stack.PushEnd(new EventPlayActionNTimes(c, 2));
    }
};

*/