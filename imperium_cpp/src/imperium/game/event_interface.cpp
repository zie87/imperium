#include <imperium/game/event_interface.hpp>

namespace imperium
{
namespace game
{
  
  event_interface::~event_interface() {}
  
  bool event_interface::is_attack() const { return false; }
  unsigned int event_interface::attacked_player() const { return npos; }

  attack_annotations* event_interface::annotations() { return nullptr; }
  bool event_interface::can_process_decisions() const { return false; }
  bool event_interface::destroy_next_event_on_stack() const { return false; }
  void event_interface::process_decision(state& , const decision_response& )  { throw "Event does not support decisions"; }

} // game
} // imperium
