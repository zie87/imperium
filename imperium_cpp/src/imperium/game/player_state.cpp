#include <imperium/game/player_state.hpp>

namespace imperium
{
namespace game
{
  card_play_info::card_play_info() {}
  card_play_info::card_play_info(card_ptr c, unsigned int tl)
  : card(c), turns_left(tl)
  {}


} // game
} // imperium