
#include <imperium/game/decisions.hpp>
#include <imperium/game/state.hpp>
#include <imperium/cards/base_card.hpp>

namespace imperium
{
namespace game
{
  void decision_state::select_cards(card_ptr card, unsigned int min_cards, unsigned int max_cards)
  {
    active_card = card;
    type = decision_type::select_cards;
    minimum_cards = min_cards;
    maximum_cards = max_cards;
    controlling_player = npos;
    card_choices.clear();
  }

  void decision_state::gain_card_from_supply( state& s, card_ptr card, 
                                              unsigned short min_cost, unsigned short max_cost, 
                                              card_filter filter)
  {
    select_cards(card, 1, 1);
    text = "Select a card to gain:";
    
    for(unsigned supply_idx = 0; supply_idx < s.data->supply_cards.size(); ++supply_idx)
    {
      auto cost = s.supply_cost(supply_idx);
      auto count = s.supply[supply_idx].count;
      auto candidate = s.data->supply_cards[supply_idx];

      bool card_acceptable = (count > 0 && cost >= min_cost && cost <= max_cost);

      if(filter == card_filter::treasure) card_acceptable &= candidate->is_treasure() ;

      if(card_acceptable)
      {
          add_unique_card(candidate);
      }
    }
  }

  void decision_state::make_discrete_choice(card_ptr c, unsigned int option_count)
  {
    active_card = c;
    type = decision_type::discrete_choice;
    minimum_cards = option_count;
    maximum_cards = option_count;
    controlling_player = npos;
  }

} // game
} // imperium


/*

bool DecisionState::IsTrivial() const
{
    if(type == DecisionSelectCards && cardChoices.Length() == 1 && minimumCards == 1 && maximumCards == 1)
    {
        return true;
    }
    return false;
}

DecisionResponse DecisionState::TrivialResponse() const
{
    DecisionResponse response;
    if(cardChoices.Length() == 1 && minimumCards == 1 && maximumCards == 1)
    {
        response.cards.Allocate(1);
        response.cards[0] = cardChoices[0];
    }
    return response;
}

void DecisionState::GainCardFromSupply(State &s, Card *c, int minCost, int maxCost, CardFilter filter)
{

}

void DecisionState::GainTreasureFromSupply(State &s, Card *c, int minCost, int maxCost)
{
    SelectCards(c, 1, 1);
    if(decisionText) text = "Select a treasure to gain:";
    for(UINT supplyIndex = 0; supplyIndex < s.data->supplyCards.Length(); supplyIndex++)
    {
        int cost = s.SupplyCost(supplyIndex);
        int count = s.supply[supplyIndex].count;
        if(count > 0 && cost >= minCost && cost <= maxCost && s.data->supplyCards[supplyIndex]->isTreasure)
        {
            AddUniqueCard(s.data->supplyCards[supplyIndex]);
        }
    }
}

void DecisionState::GainVictoryFromSupply(State &s, Card *c, int minCost, int maxCost)
{
    SelectCards(c, 1, 1);
    if(decisionText) text = "Select a treasure to gain:";
    for(UINT supplyIndex = 0; supplyIndex < s.data->supplyCards.Length(); supplyIndex++)
    {
        int cost = s.SupplyCost(supplyIndex);
        int count = s.supply[supplyIndex].count;
        if(count > 0 && cost >= minCost && cost <= maxCost && s.data->supplyCards[supplyIndex]->isVictory)
        {
            AddUniqueCard(s.data->supplyCards[supplyIndex]);
        }
    }
}

*/