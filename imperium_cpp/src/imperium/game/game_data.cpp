#include <imperium/game/game_data.hpp>

namespace imperium
{
namespace game
{

  void buy_ledger::reset() { cards_bought.clear(); }

  void buy_ledger::record_buy(card_ptr c)
  {
    for( auto it : cards_bought) if( it == c ) return;
    cards_bought.push_back(c);
  }

  player_info::player_info() {}
  player_info::player_info(unsigned int idx, const std::string& n, player* c)
  : index(idx), name(n), controller(c)
  {}

  void game_data::new_game(const info_vec& playerList, const game_options& opts)
  {
    // PersistentAssert(playerList[0].index == 0 && playerList[1].index == 1, "Player indices are invalid");

    options = opts;
    players = playerList;

    // supplyCards.FreeMemory();
    // supplyCards.PushEnd(cards->get_card("copper"));
    // supplyCards.PushEnd(cards->get_card("silver"));
    // supplyCards.PushEnd(cards->get_card("gold"));

    // if(options.prosperity) supplyCards.PushEnd(cards->get_card("platinum"));

    // supplyCards.PushEnd(cards->get_card("estate"));
    // supplyCards.PushEnd(cards->get_card("duchy"));
    // supplyCards.PushEnd(cards->get_card("province"));

    // if(options.prosperity) supplyCards.PushEnd(cards->get_card("colony"));

    // supplyCards.PushEnd(cards->get_card("curse"));

    // for(Card *c : options.supplyPiles) supplyCards.PushEnd(c);

    // reactionCards.FreeMemory();
    // for(Card *c : supplyCards)
    // {
    //     if(c->isReaction) reactionCards.PushEnd(c);
    // }

    for(auto it : players) it.ledger.reset();

    // coppersmithInSupply = supplyCards.Contains(cards->get_card("coppersmith"));
    // bridgeInSupply = supplyCards.Contains(cards->get_card("bridge"));
    // quarryInSupply = supplyCards.Contains(cards->get_card("quarry"));
    // talismanInSupply = supplyCards.Contains(cards->get_card("talisman"));
    // hoardInSupply = supplyCards.Contains(cards->get_card("hoard"));
    // goonsInSupply = supplyCards.Contains(cards->get_card("goons"));
    // grandMarketInSupply = supplyCards.Contains(cards->get_card("grand market"));
    // treasuryInSupply = supplyCards.Contains(cards->get_card("treasury"));
    // watchtowerInSupply = supplyCards.Contains(cards->get_card("watchtower"));
    // royalSealInSupply = supplyCards.Contains(cards->get_card("royal seal"));
    // tradeRouteInSupply = supplyCards.Contains(cards->get_card("trade route"));
    // mintInSupply = supplyCards.Contains(cards->get_card("mint"));
    // championInSupply = supplyCards.Contains(cards->get_card("champion"));
    // gardenerInSupply = supplyCards.Contains(cards->get_card("gardener"));
    // grandCourtInSupply = supplyCards.Contains(cards->get_card("grand court"));
    // plunderInSupply = supplyCards.Contains(cards->get_card("plunder"));
    // promisedLandInSupply = supplyCards.Contains(cards->get_card("promised land"));
    // furnaceInSupply = supplyCards.Contains(cards->get_card("furnace"));

    // useGainList = supplyCards.Contains(cards->get_card("smugglers")) || supplyCards.Contains(cards->get_card("treasury"));
  }

  void game_data::init(const cards::card_database& db)
  {
    cards = &db;
    init_base_cards();
  }

  void game_data::init_base_cards()
  {
    base_cards.copper   = cards->get_card("copper");
    base_cards.silver   = cards->get_card("silver");
    base_cards.gold     = cards->get_card("gold");
    base_cards.platinum = cards->get_card("platinum");
    base_cards.potion   = cards->get_card("potion");
    base_cards.estate   = cards->get_card("estate");
    base_cards.duchy    = cards->get_card("duchy");
    base_cards.province = cards->get_card("province");
    base_cards.colony   = cards->get_card("colony");
    base_cards.curse    = cards->get_card("curse");

    // base_cards.moat        = cards->get_card("moat");
    // base_cards.lighthouse  = cards->get_card("lighthouse");
    // base_cards.coppersmith = cards->get_card("coppersmith");
    base_cards.bridge      = cards->get_card("bridge");
    base_cards.quarry      = cards->get_card("quarry");
    // base_cards.talisman    = cards->get_card("talisman");
    // base_cards.hoard       = cards->get_card("hoard");
    // base_cards.goons       = cards->get_card("goons");

    base_cards.peddler     = cards->get_card("peddler");
    // base_cards.bank        = cards->get_card("bank");
    base_cards.royal_seal  = cards->get_card("royal seal");
    base_cards.watchtower  = cards->get_card("watchtower");
    // base_cards.treasury    = cards->get_card("treasury");
    // base_cards.treasureMap = cards->get_card("treasure map");
    // base_cards.tradeRoute  = cards->get_card("trade route");
    // base_cards.grandMarket = cards->get_card("grand market");
    // base_cards.mint        = cards->get_card("mint");

    // base_cards.champion      = cards->get_card("champion");
    // base_cards.gardener      = cards->get_card("gardener");
    // base_cards.grandCourt    = cards->get_card("grand court");
    base_cards.plunder       = cards->get_card("plunder");
    // base_cards.promisedLand  = cards->get_card("promised land");
    // base_cards.trailblazer   = cards->get_card("trailblazer");

    // base_cards.evangelist  = cards->get_card("evangelist");
    // base_cards.furnace     = cards->get_card("furnace");
    // base_cards.witchdoctor = cards->get_card("witchdoctor");
  }
} // game
} // imperium

/*

#include "Main.h"

String GameOptions::ToString() const
{
    String s;
    for(Card *c : supplyPiles) s += c->name + ",";
    s.PopEnd();

    if(startingCondition == StartingCondition25Split) s += "@25Split";
    else if(startingCondition == StartingCondition34Split) s += "@34Split";
    else if(startingCondition == StartingConditionRandom) s += "@RandomSplit";

    if(prosperity) s += "@Prosperity";
    else s += "@NoProsperity";

    return s;
}

Card* GameOptions::RandomSupplyCard(const CardDatabase &cards) const
{
    int baseSupplyCount = 7;
    if(prosperity) baseSupplyCount += 2;
    int supplyPileCount = supplyPiles.Length() + baseSupplyCount;
    
    UINT supplyIndex = rand() % supplyPileCount;
    if(supplyIndex < supplyPiles.Length())
    {
        return supplyPiles[supplyIndex];
    }
    else if(prosperity)
    {
        int baseSupplyIndex = supplyIndex - supplyPiles.Length();
        if(baseSupplyIndex == 0) return cards.get_card("copper");
        if(baseSupplyIndex == 1) return cards.get_card("silver");
        if(baseSupplyIndex == 2) return cards.get_card("gold");
        if(baseSupplyIndex == 3) return cards.get_card("platinum");
        if(baseSupplyIndex == 4) return cards.get_card("estate");
        if(baseSupplyIndex == 5) return cards.get_card("duchy");
        if(baseSupplyIndex == 6) return cards.get_card("province");
        if(baseSupplyIndex == 7) return cards.get_card("colony");
        if(baseSupplyIndex == 8) return cards.get_card("curse");
    }
    else
    {
        int baseSupplyIndex = supplyIndex - supplyPiles.Length();
        if(baseSupplyIndex == 0) return cards.get_card("copper");
        if(baseSupplyIndex == 1) return cards.get_card("silver");
        if(baseSupplyIndex == 2) return cards.get_card("gold");
        if(baseSupplyIndex == 3) return cards.get_card("estate");
        if(baseSupplyIndex == 4) return cards.get_card("duchy");
        if(baseSupplyIndex == 5) return cards.get_card("province");
        if(baseSupplyIndex == 6) return cards.get_card("curse");
    }
    SignalError("Card not found");
    return NULL;
}

void GameOptions::SetupGame(const CardDatabase &cards, const String &options)
{
    Vector<String> parts = options.Partition("|");
    
    Vector<String> requiredCards = parts[0].Partition(",");
    for(String &s : requiredCards)
    {
        while(s.StartsWith(" ") && s.Length() > 0) s.PopFront();
        while(s.EndsWith(" ") && s.Length() > 0) s.PopEnd();
    }

    if(parts[1] == "34Split") startingCondition = StartingCondition34Split;
    if(parts[1] == "25Split") startingCondition = StartingCondition25Split;

    prosperity = (parts[2] == "Prosperity");

    RandomizeSupplyPiles(cards, requiredCards);
}

void GameOptions::RandomizeSupplyPiles(const CardDatabase &cards)
{
    //
    // Standard dominion rules suggest that prosperity should be N * 10%, where N is the # of prosperity cards
    // I just choose it to be 33% of the time.
    //
    prosperity = (rand() % 3 == 0);

    if(rand() % 6 == 0) startingCondition = StartingCondition25Split;
    else startingCondition = StartingCondition34Split;

    Vector<String> requiredCards;
    RandomizeSupplyPiles(cards, requiredCards);
}

void GameOptions::RandomizeSupplyPiles(const CardDatabase &cards, const Vector<String> &requiredCards)
{
    for(UINT cardIndex = 0; cardIndex < requiredCards.Length() && cardIndex < 10; cardIndex++)
    {
        Card *requiredCard = cards.get_card(requiredCards[cardIndex]);
        if(requiredCard != NULL) supplyPiles.PushEnd(requiredCard);
    }
    while(supplyPiles.Length() < 10)
    {
        Card *newCard;
        do
        {
            newCard = cards.RandomSupplyCard();
        } while(supplyPiles.Contains(newCard));
        supplyPiles.PushEnd(newCard);
    }
    supplyPiles.Sort([](const Card *a, const Card *b)
    { 
        if(a->cost < b->cost) return true;
        if(a->cost > b->cost) return false;
        return (a->name < b->name);
    });
}








*/