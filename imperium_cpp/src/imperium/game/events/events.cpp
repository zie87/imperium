#include <imperium/game/events/events.hpp>
#include <imperium/game/state.hpp>
#include <imperium/game/player_state.hpp>
#include <imperium/game/decisions.hpp>

#include <algorithm>

namespace imperium
{
namespace game
{
namespace events
{

  //////////////////////////////////////////////////////////////////////////////
  // TRASH CARD FROM PLAY
  //////////////////////////////////////////////////////////////////////////////
  trash_card_from_play::trash_card_from_play(unsigned int p, card_ptr c)
  : player(p), card(c)
  {}

  bool trash_card_from_play::advance(state &s)
  {
    using info_vec = std::vector<card_play_info>;

    info_vec::iterator play_area_idx = s.players[player].play_area.end();
    for(auto it = s.players[player].play_area.begin(); it != s.players[player].play_area.end(); ++it )
    {
      if( it->card == card ) play_area_idx = it;
    }

    if(play_area_idx == s.players[player].play_area.end())
    {
      // TODO: change to real log
      std::cout << "player " << player << " fails to trash " << card->name << std::endl;  
    }
    else
    {
      // TODO: change to real log
      std::cout << "player " << player << " trashes " << card->name << std::endl;  
      s.players[player].play_area.erase(play_area_idx);
    }
    return true;
  }

  //////////////////////////////////////////////////////////////////////////////
  // TRASH CARD FROM HAND
  //////////////////////////////////////////////////////////////////////////////
  trash_card_from_hand::trash_card_from_hand(unsigned int p, card_ptr c)
  : player(p), card(c)
  {}

  bool trash_card_from_hand::advance(state &s)
  {
    auto hand_idx = std::find(s.players[player].hand.begin(), s.players[player].hand.end(), card);
    if( hand_idx == s.players[player].hand.end() ) 
    {  
      // TODO: change to real log
      std::cout << "player " << player << " fails to trash " << card->name << std::endl;  
    } else
    {
      // TODO: change to real log
      std::cout << "player " << player << " trashes " << card->name << std::endl;  
      s.players[player].hand.erase(hand_idx);
      // Normally, this is where we would add the card to the trash. However, I have not yet decided to add the trash to the state,
      // although I will need to for expansions like Dark Ages.
    }

    return true;
  }

  //////////////////////////////////////////////////////////////////////////////
  // DRAW CARD
  //////////////////////////////////////////////////////////////////////////////
  draw_card::draw_card(unsigned int p)
  : player(p)
  {}

  bool draw_card::advance(state &s)
  {
    s.draw_card(player);
    return true;
  }

  //////////////////////////////////////////////////////////////////////////////
  // DISCARD CARD
  //////////////////////////////////////////////////////////////////////////////
  discard_card::discard_card(unsigned int p, card_ptr c, discard_zone z)
  :player(p), card(c), zone(z)
  {}

  bool discard_card::advance(state &s)
  {
    auto hand_idx = std::find( s.players[player].hand.begin(), s.players[player].hand.end(), card );
    if( hand_idx != s.players[player].hand.end() && zone == discard_zone::hand )
    {
      // LOGGING
      std::cout << "[discard card] player " << player << " fails to discard " << card->name << std::endl;
    } else
    {
      // LOGGING
      std::cout << "[discard card] player " << player << " discards " << card->name << std::endl;
      if(zone == discard_zone::hand )
      {
        s.players[player].hand.erase(hand_idx);
      }
      s.players[player].discard_pile.push_back(card);
    }
    return true;
  }

  //////////////////////////////////////////////////////////////////////////////
  // GAIN CARD
  //////////////////////////////////////////////////////////////////////////////

  gain_card::gain_card(unsigned short p, card_ptr c)
  : player(p), card(c), bought(false)
  , state(trigger_state::none), zone(gain_zone::to_discard), m_is_attack(false)
  {}

  gain_card::gain_card(unsigned short p, card_ptr c, bool b, bool ia, gain_zone z)
  : player(p), card(c), bought(b),state(trigger_state::none), zone(z),  m_is_attack(ia)
  {}

  bool gain_card::is_attack() const { return m_is_attack; }
  unsigned int gain_card::attacked_player() const  { return player; }
  attack_annotations* gain_card::annotations() { return &m_annotations; }

  bool gain_card::can_process_decisions() const { return true; }

  void gain_card::process_decision(game::state& , const decision_response& response)
  {
    if(state == trigger_state::processing_royal_seal)
    {
      if(response.choice == 0)
      {
        std::cout << "[gain_card] uses Royal Seal";
        zone = gain_zone::to_deck_top;
      }
      else { std::cout << "[gain_card] don't use Royal Seal"; }
    }
    else if(state == trigger_state::processing_watchtower)
    {
      if(response.choice == 1)
      {
        std::cout << "[gain_card] player " << player << " reveals watchtower" << std::endl;
        zone = gain_zone::to_deck_top;
      }
      else if(response.choice == 0)
      {
        std::cout << "[gain_card] player " << player << " reveals watchtower" << std::endl;
        std::cout << "[gain_card] player " << player << " trashes " << card->name << std::endl;
        zone = gain_zone::to_trash;
      }
      else if(response.choice == 2)
      {
        std::cout << "[gain_card] player " << player << " doesn't reveals watchtower" << std::endl;
      }
    }
    state = trigger_state::processed;
  }

  bool gain_card::advance(game::state &s)
  {
    supply_entry& cur_supply = s.supply[s.data->supply_index(card)];
    player_state& p = s.players[player];
    if(cur_supply.count > 0)
    {
      if((s.data->watchtower_in_supply || s.data->royal_seal_in_supply) && state == trigger_state::none)
      {
        bool has_watchtower = false;
        for( auto it : p.hand ) if(it->name == "watchtower" ) has_watchtower = true;
        if( has_watchtower )
        {
          state = trigger_state::processing_watchtower;
          s.decision.make_discrete_choice(s.data->base_cards.watchtower, 3);
          s.decision.controlling_player = player;
          s.decision.text = "About to gain" + card->name +":|Trash it|Put it on top of deck|Don't use Watchtower";
          return false;
        }
        else if(p.card_in_play(s.data->base_cards.royal_seal))
        {
          state = trigger_state::processing_royal_seal;
          s.decision.make_discrete_choice(s.data->base_cards.royal_seal, 2);
          s.decision.text = "Put" + card->name + "on top of your deck?|Yes|No";
          return false;
        }
      }

      if(s.data->use_gain_list && player == s.player) s.gain_list.push_back(card);
      if(s.data->trade_route_in_supply)
      {
          supply_entry& entry = s.supply[s.data->supply_index(card)];
          if(entry.trade_route_token == 1)
          {
            entry.trade_route_token = 0;
            s.trade_route_value++;
          }
      }

      std::string zone_modifier;
      if(zone == gain_zone::to_hand)
      {
          p.hand.push_back(card);
          zone_modifier = " in hand";
      }
      else if(zone == gain_zone::to_discard || zone == gain_zone::to_discard_ironworks)
      {
        p.discard_pile.push_back(card);
        if(zone == gain_zone::to_discard_ironworks)
        {
          if(card->is_action() ) p.actions++;
          if(card->is_treasure() )
          {
            std::cout << "[gain_card] player " << player << " gets $1" << std::endl;
            p.money++;
          }
          if(card->is_victory()) s.stack.push_back( std::shared_ptr<event_interface>(new draw_card(s.player)) );
        }
      }
      else if(zone == gain_zone::to_deck_top)
      {
        p.deck.push_back(card);
        zone_modifier = " and puts it on their deck";
      }
      cur_supply.count--;
      if(bought)
      {
        auto cost = s.supply_cost(card);
        std::cout << "[gain_card] player " << player << " buys " << card->name << std::endl;
        std::cout << "[gain_card] player " << player << " spend $ " << std::to_string(cost) << std::endl;
        p.money -= cost;
        p.buys--;
      }
      else if(zone != gain_zone::to_trash)
      {
        std::cout << "[gain_card] player " << player << " gains a " << card->name << zone_modifier << std::endl;
      }
    }
    else
    {
      if(bought)
      {
        // We will sometimes get here if the player asks for multiple piles of the same type
        std::cout << "[gain_card] player " << player << " cannot buy " << card->name << std::endl;
        p.buys--;
      }
      else
      {
        std::cout << "[gain_card] player " << player << " cannot gain " << card->name << std::endl;
      }
    }
    return true;
  }

  //////////////////////////////////////////////////////////////////////////////
  // DISCARD DOWN TO N
  //////////////////////////////////////////////////////////////////////////////
  discard_down_to_n::discard_down_to_n(card_ptr s, unsigned short p, unsigned short h)
  : source(s), player(p), hand_size(h)
  {}

  bool discard_down_to_n::is_attack() const { return true; }
  unsigned int discard_down_to_n::attacked_player() const  { return player; }
  attack_annotations* discard_down_to_n::annotations() { return &m_annotations; }

  bool discard_down_to_n::can_process_decisions() const { return true; }
  void discard_down_to_n::process_decision(game::state& s, const decision_response& response)
  {
    for(auto it : response.cards)
    {
      s.stack.push_back(std::shared_ptr<event_interface>(new discard_card(s.decision.controlling_player, it, discard_zone::hand)) );
    }
    done = true;
  }

  bool discard_down_to_n::advance(game::state &s)
  {
    if(done) return true;

    unsigned int cur_hand_size =  s.players[player].hand.size() ;
    int cards_to_discard = cur_hand_size - hand_size;
    if(cards_to_discard <= 0)
    {
      std::cout << "[discard_down_to_n] player " << player << "has " + std::to_string(cur_hand_size) + " cards in hand" << std::endl;
      return true;
    }

    s.decision.select_cards(source, cards_to_discard, cards_to_discard);
    s.decision.card_choices = s.players[player].hand;
    s.decision.controlling_player = player;

    if(cards_to_discard == 1) s.decision.text = "Choose a card to discard:";
    else s.decision.text = "Choose " + std::to_string(cards_to_discard) + " cards to discard:";

    return false;
  }


} // events
} // game
} // imperium

/*



*/