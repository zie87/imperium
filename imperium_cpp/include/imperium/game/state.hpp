#ifndef IMPERIUM_GAME_STATE_HPP
#define IMPERIUM_GAME_STATE_HPP

#include <imperium/cards/base_card.hpp>

#include <imperium/game/decisions.hpp>
#include <imperium/game/game_data.hpp>
#include <imperium/game/player_state.hpp>
#include <imperium/game/event_interface.hpp>

#include <deque>
#include <vector>
#include <memory>

namespace imperium
{
namespace game
{
  struct player_state;


  enum game_phase
  {
    action,
    treasure,
    buy,
    cleanup
  };


  struct supply_entry
  {
    unsigned short count;
    unsigned short trade_route_token;
  };

  struct state
  {
    using event_ptr = std::shared_ptr<event_interface>;
    using card_ptr  = std::shared_ptr<cards::base_card>;

    // utility
    void shuffle(unsigned int player_idx);
    card_ptr draw_card(unsigned int player_idx);
    void draw_cards(unsigned int player_idx, unsigned int card_count);
    void discard_card(unsigned int player_idx, card_ptr c);

    // Query
    unsigned short supply_cost(int supply_idx) const;
    unsigned short supply_cost(card_ptr c) const;
    unsigned short supply_count(card_ptr c) const;
    unsigned short empty_supply_piles() const;

    // states
    unsigned short player = 0;
    decision_state decision;
    const game_data* data;
    std::vector<player_state> players;
    game_phase phase;

    // List of cards gained this turn
    std::vector<card_ptr> gain_list;
    
    // Events are entities that either require a decision from the player, or 
    // may trigger a response from the user.
    // If stack is empty, then the active decision is determined by phase.
    // If stack is non-empty, then the active decision is determined by the top of the stack.
    std::deque<event_ptr>     stack;
    std::vector<supply_entry> supply;
  
    short trade_route_value = 0;
  };

} // game
} // imperium

#endif // IMPERIUM_GAME_STATE_HPP

/*
struct State
{
    static const UINT playerMaximum = 2;
    static const UINT maxSupply = 20;

    //
    // Game Control
    //
    void NewGame(const GameData &_data);
    void AdvanceToNextDecision(UINT recursionDepth);
    void AdvancePhase();
    void ProcessDecision(const DecisionResponse &response);

    //
    // Utility
    //

    void PlayCard(UINT playerIndex, Card *c);
    void ProcessAction(Card *c);
    void ProcessTreasure(Card *c);
    void CheckEndConditions();
    void ReorderDeck(Card *source, UINT playerIndex, UINT cardCount);
    
    //
    // Query
    //
    bool GameDone() const;
    int PlayerScore(UINT playerIndex) const;
    UINT ActionsPlayedThisTurn() const;
    Vector<int> WinningPlayers() const;

    //
    // Logging
    //
    void Log(UINT playerIndex, const String &s);
    void Log(const String &s);
    void LogIndent(UINT indentLevel, UINT playerIndex, const String &s);
    void LogIndent(UINT indentLevel, const String &s);

    //
    // Internal triggers
    //
    void BuyPhaseStart();

    //
    // State
    //




    //
    // List of cards gained by the previous player
    //
    Vector<Card*> prevGainList;

};

*/