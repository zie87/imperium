#ifndef IMPERIUM_GAME_PLAYER_STATE_HPP
#define IMPERIUM_GAME_PLAYER_STATE_HPP

#include <imperium/cards/base_card.hpp>

#include <vector>
#include <memory>

namespace imperium
{
namespace game
{
  struct card_play_info
  {
    using card_ptr = std::shared_ptr<cards::base_card>;

    card_play_info();
    explicit card_play_info(card_ptr c, unsigned int tl);

    card_ptr card = card_ptr(nullptr);

    // This is only used by throne room and king's court. 
    // We can't use "bound to" because of throne rooming throne rooms, 
    // the 2nd time a TR is played, the throne room would have to be rebound.
    unsigned int copies = 0;

    // These are used by durations
    unsigned int turns_left = 0;
  };

  struct player_state
  {
    using card_ptr = std::shared_ptr<cards::base_card>;
    using card_vec = std::vector<card_ptr>;
    using info_vec = std::vector<card_play_info>;

    bool card_in_play(card_ptr c) const
    {
      for(const card_play_info& play : play_area) if(play.card == c) return true;
      return false;
    }

    card_vec hand;
    card_vec deck;
    card_vec discard_pile;
    info_vec play_area;

    unsigned int actions    = 0;
    unsigned int buys       = 0;
    unsigned int money      = 0;
    unsigned int v_tokens   = 0;
    unsigned int turns      = 0;
  };

} // game
} // imperium

#endif // IMPERIUM_GAME_PLAYER_STATE_HPP

/*
struct PlayerState
{
    void NewGame(const GameData &data);
    int ActionCount() const;
    int TreasureCount() const;
    int VictoryCount() const;
    int TotalCards() const;
    int MoneyTotal() const;

    //map<Card*,int> CardCounts() const;

    Vector<Card*> islandZone;

};
*/