#ifndef IMPERIUM_GAME_DECISIONS_HPP
#define IMPERIUM_GAME_DECISIONS_HPP

#include <memory>
#include <vector>
#include <string>
#include <algorithm>

namespace imperium
{
  namespace cards
  {
    class base_card;
  } // cards;

namespace game
{
  struct state;

  enum class decision_type
  {
    none,
    select_cards,
    discrete_choice,
    game_over,
  };

  enum class card_filter
  {
    any,
    treasure,
  };

  struct decision_response
  {
    using card_ptr = std::shared_ptr<cards::base_card>;
    using card_vec = std::vector<card_ptr>;
    static const unsigned int npos = -1; 

    unsigned int choice = npos;
    card_ptr single_card = card_ptr(nullptr);
    card_vec cards;
  };

  class decision_state
  {
    public:
      using card_ptr = std::shared_ptr<cards::base_card>;
      using card_vec = std::vector<card_ptr>;
      static const unsigned int npos = -1; 

      void select_cards(card_ptr card, unsigned int min_cards, unsigned int max_cards);
      void gain_card_from_supply(state& s, card_ptr card, unsigned short min_cost, unsigned short max_cost, card_filter filter = card_filter::any);

      void make_discrete_choice(card_ptr c, unsigned int option_count);

      inline void add_unique_card(card_ptr c)
      {
        if( std::find( card_choices.begin(), card_choices.end(), c ) != card_choices.end() ) card_choices.push_back(c);
      }

      card_ptr      active_card = card_ptr(nullptr);
      card_vec      card_choices;
      decision_type type        = decision_type::none;

      unsigned int minimum_cards = 0;
      unsigned int maximum_cards = 0;

      // if controllingPlayer is npos, the active player is the one making the decision.
      unsigned int controlling_player = npos;
      std::string text = std::string();
  };

} // game
} // imperium

#endif // IMPERIUM_GAME_DECISIONS_HPP

/*

struct DecisionResponse
{
    DecisionResponse()
    {
        choice = -1;
        singleCard = NULL;
    }

    UINT choice;
    Card *singleCard;
    Vector<Card*> cards;
};

class DecisionState
{
public:
    bool IsTrivial() const;
    DecisionResponse TrivialResponse() const;
    


    void GainCardFromSupply(State &s, Card *c, int minCost, int maxCost, CardFilter filter = FilterAny);
    void GainTreasureFromSupply(State &s, Card *c, int minCost, int maxCost);
    void GainVictoryFromSupply(State &s, Card *c, int minCost, int maxCost);

    __forceinline void AddUniqueCards(Vector<Card*> cards)
    {
        for(Card *c : cards) AddUniqueCard(c);
    }

    DecisionType type;
    Card *activeCard;

    Vector<Card*> cardChoices;
    UINT minimumCards;
    UINT maximumCards;



    //
    // Options are separated by |
    //
    String text;
};

*/