#ifndef IMPERIUM_GAME_EVENTS_HPP
#define IMPERIUM_GAME_EVENTS_HPP

#include <imperium/cards/base_card.hpp>
#include <imperium/game/event_interface.hpp>

namespace imperium
{
namespace game
{
  struct state;
namespace events
{

enum class discard_zone
{
  hand,
  side_zone,
};

class trash_card_from_play : public event_interface
{
  public:
    using card_ptr = std::shared_ptr<cards::base_card>;
    trash_card_from_play(unsigned int p, card_ptr c);
    bool advance(state &s) override;

    unsigned int player;
    card_ptr card;
};

class trash_card_from_hand : public event_interface
{
  public:
    using card_ptr = std::shared_ptr<cards::base_card>;
    trash_card_from_hand(unsigned int p, card_ptr c);
    bool advance(state &s) override;

    unsigned int player;
    card_ptr card;
};

// Making card draw an effect is easy but inefficient since it is only needed for Stash, a rather inelegant card I don't plan to implement.
// Still, card draw can be pushed onto the stack by cards such as Cellar. This is because you need to process the discards before drawing,
// so that the discarded cards can be drawn.
//
// ** Possible triggers
// Stash
class draw_card : public event_interface
{
  public:
    draw_card(unsigned int p);
    bool advance(state &s) override;

    unsigned int player;
};


// When fromSideZone is true, the card is pulled from a special reserved zone instead of the hand.
//
// ** Possible triggers
// Tunnel
class discard_card : public event_interface
{
  public:
    using card_ptr = std::shared_ptr<cards::base_card>;
    discard_card(unsigned int p, card_ptr c, discard_zone z);
    bool advance(state &s) override;

    unsigned int player;
    card_ptr     card;
    discard_zone zone;
};


// ** Possible triggers
// Watchtower
// Royal seal
// Mint
class gain_card : public event_interface
{
  public:
    using card_ptr = std::shared_ptr<cards::base_card>;

    gain_card(unsigned short p, card_ptr c);
    gain_card(unsigned short p, card_ptr c, bool b, bool ia, gain_zone z);

    bool is_attack() const override;
    unsigned int attacked_player() const override;
    attack_annotations* annotations() override;

    bool can_process_decisions() const override;
    void process_decision(state& s, const decision_response& response) override;
    bool advance(state &s) override;

    enum class trigger_state
    {
      none,
      processed,
      processing_royal_seal,
      processing_watchtower,
    };

    unsigned short player;
    card_ptr card;
    bool bought;
    trigger_state state;
    gain_zone zone;

    bool m_is_attack;
    attack_annotations m_annotations = attack_annotations();
};


class discard_down_to_n : public event_interface
{
  public:
    using card_ptr = std::shared_ptr<cards::base_card>;

    discard_down_to_n(card_ptr s, unsigned short p, unsigned short h);

    bool is_attack() const override;
    unsigned int attacked_player() const override;
    attack_annotations* annotations() override;

    bool can_process_decisions() const override;
    void process_decision(state& s, const decision_response& response) override;
    bool advance(state &s) override;

    card_ptr source;
    unsigned short player   = 0;
    unsigned short hand_size = 0;
    attack_annotations m_annotations = attack_annotations();
    bool done = false;
};

} // events
} // game
} // imperium
#endif // IMPERIUM_GAME_EVENTS_HPP

/*



class EventTrashCardFromDeck : public Event
{
public:
    EventTrashCardFromDeck(UINT _player)
    {
        player = _player;
    }
    bool Advance(State &s)
    {
        Card *c = s.players[player].deck.Last();
        if(logging) s.LogIndent(1, player, "trashes " + c->PrettyName() + " from the top of their deck");
        s.players[player].deck.PopEnd();
        return true;
    }

    UINT player;
};

class EventTrashCardFromSideZone : public Event
{
public:
    EventTrashCardFromSideZone(UINT _player, Card *_c)
    {
        player = _player;
        c = _c;
    }
    bool Advance(State &s)
    {
        if(logging) s.LogIndent(1, player, "trashes " + c->PrettyName());
        return true;
    }

    UINT player;
    Card *c;
};






class EventPutOnDeckDownToN : public Event
{
public:
    EventPutOnDeckDownToN(Card *_source, UINT _player, UINT _handSize)
    {
        source = _source;
        player = _player;
        handSize = _handSize;
        done = false;
    }
    bool IsAttack() const
    {
        return true;
    }
    int AttackedPlayer() const
    {
        return player;
    }
    AttackAnnotations* Annotations()
    {
        return &annotations;
    }
    bool Advance(State &s)
    {
        if(done) return true;

        int currentHandSize = int(s.players[player].hand.Length());
        int cardsToDiscard = currentHandSize - handSize;
        if(cardsToDiscard <= 0)
        {
            if(logging) s.Log(player, "has " + String(currentHandSize) + " cards in hand");
            return true;
        }

        s.decision.SelectCards(source, cardsToDiscard, cardsToDiscard);
        s.decision.cardChoices = s.players[player].hand;
        s.decision.controllingPlayer = player;
        if(decisionText)
        {
            if(cardsToDiscard == 1) s.decision.text = "Choose a card to put on top of your deck:";
            else s.decision.text = "Choose " + String(cardsToDiscard) + " cards to put on top of your deck:";
        }
        return false;
    }
    bool CanProcessDecisions() const
    {
        return true;
    }
    void ProcessDecision(State &s, const DecisionResponse &response)
    {
        PlayerState &p = s.players[player];
        for(Card *c : response.cards)
        {
            if(logging) s.LogIndent(1, player, "puts " + c->PrettyName() + " on top of their deck");
            p.hand.RemoveSwap(p.hand.FindFirstIndex(c));
            p.deck.PushEnd(c);
        }
        done = true;
    }

    Card *source;
    UINT player;
    UINT handSize;
    AttackAnnotations annotations;
    bool done;
};

class EventDiscardNCards : public Event
{
public:
    EventDiscardNCards(Card *_source, UINT _player, int _minDiscardCount, int _maxDiscardCount)
    {
        source = _source;
        player = _player;
        minDiscardCount = _minDiscardCount;
        maxDiscardCount = _maxDiscardCount;
        done = false;
    }
    bool Advance(State &s)
    {
        if(done) return true;

        int currentHandSize = int(s.players[player].hand.Length());
        int cardsToDiscard = Math::Min(currentHandSize, maxDiscardCount);
        minDiscardCount = Math::Min(minDiscardCount, cardsToDiscard);
        if(cardsToDiscard == 0)
        {
            if(logging) s.Log(player, "has no cards to discard");
            return true;
        }

        s.decision.SelectCards(source, minDiscardCount, cardsToDiscard);
        s.decision.cardChoices = s.players[player].hand;
        s.decision.controllingPlayer = player;
        if(decisionText)
        {
            if(cardsToDiscard == 1) s.decision.text = "Choose a card to discard:";
            else s.decision.text = "Choose " + String(cardsToDiscard) + " cards to discard:";
        }
        return false;
    }
    bool CanProcessDecisions() const
    {
        return true;
    }
    void ProcessDecision(State &s, const DecisionResponse &response)
    {
        for(Card *c : response.cards)
        {
            s.stack.PushEnd(new EventDiscardCard(s.decision.controllingPlayer, c, DiscardFromHand));
        }
        if(source == s.data->baseCards.furnace && response.cards.Length() > 0)
        {
            if(logging) s.LogIndent(1, "gets $1 from " + source->PrettyName());
            s.players[s.player].money++;
        }
        done = true;
    }

    Card *source;
    UINT player;
    int minDiscardCount, maxDiscardCount;
    bool done;
};

class EventPlayActionNTimes : public Event
{
public:
    EventPlayActionNTimes(Card *_source, UINT _count)
    {
        source = _source;
        target = NULL;
        count = _count;
    }
    bool CanProcessDecisions() const
    {
        return true;
    }
    bool Advance(State &s)
    {
        if(count == 0) return true;
        PlayerState &p = s.players[s.player];

        if(target == NULL)
        {
            if(p.ActionCount() == 0)
            {
                if(logging) s.Log("has no actions to play");
                return true;
            }

            s.decision.SelectCards(source, 1, 1);
            if(decisionText) s.decision.text = "Select an action to play " + String(count) + " times:";
            if(source == s.data->baseCards.trailblazer)
            {
                for(Card *c : p.hand)
                {
                    if(c->isAction && s.SupplyCost(c) <= 3) s.decision.AddUniqueCard(c);
                }
                if(s.decision.cardChoices.Length() == 0)
                {
                    s.decision.type = DecisionNone;
                    if(logging) s.Log("has no actions to play");
                    return true;
                }
            }
            else
            {
                for(Card *c : p.hand)
                {
                    if(c->isAction) s.decision.AddUniqueCard(c);
                }
            }
        }
        else
        {
            if(logging) s.Log("plays " + target->PrettyName());
            count--;
            s.ProcessAction(target);
        }
        return false;
    }
    void ProcessDecision(State &s, const DecisionResponse &response)
    {
        PlayerState &p = s.players[s.player];
        target = response.cards[0];
        int index = p.hand.FindFirstIndex(target);
        p.hand.RemoveSwap(index);

        if(source == s.data->baseCards.witchdoctor)
        {
            int supplyCount = s.SupplyCount(target);
            if(supplyCount == 0)
            {
                if(logging) s.LogIndent(1, "cannot gain " + target->PrettyName());
                count = 1;
            }
            else
            {
                s.stack.PushEnd(new EventGainCard(s.player, target, false, false, GainToDiscard));
            }
        }

        CardPlayInfo newInfo(target, 1);
        newInfo.copies = count;

        p.playArea.PushEnd(newInfo);
    }

    UINT count;
    Card *source, *target;
};

class EventChooseCardsToTrash : public Event
{
public:
    EventChooseCardsToTrash(Card *_source, int _minCards, int _maxCards)
    {
        done = false;
        source = _source;
        minCards = _minCards;
        maxCards = _maxCards;
    }
    bool CanProcessDecisions() const
    {
        return true;
    }
    bool Advance(State &s)
    {
        if(done) return true;
        minCards = Math::Min(minCards, (int)s.players[s.player].hand.Length());
        maxCards = Math::Min(maxCards, (int)s.players[s.player].hand.Length());

        if(maxCards == 0)
        {
            if(logging) s.LogIndent(1, "has no cards to trash");
            s.decision.type = DecisionNone;
            return true;
        }

        s.decision.SelectCards(source, minCards, maxCards);
        if(decisionText)
        {
            if(minCards == maxCards && minCards == 1) s.decision.text = "Choose a card to trash:";
            else if(minCards == maxCards) s.decision.text = "Choose " + String(minCards) + " cards to trash:";
            else if(minCards == 0) s.decision.text = "Choose up to " + String(maxCards) + " cards to trash:";
            else s.decision.text = "Choose cards to trash:";
        }
        s.decision.cardChoices = s.players[s.player].hand;
        return false;
    }
    void ProcessDecision(State &s, const DecisionResponse &response)
    {
        PlayerState &p = s.players[s.player];
        for(Card *c : response.cards)
        {
            s.stack.PushEnd(new EventTrashCardFromHand(s.player, c));
        }

        if(source == s.data->baseCards.evangelist && response.cards.Length() == 0)
        {
            s.stack.PushEnd(new EventTrashCardFromPlay(s.player, s.data->baseCards.evangelist));
        }

        done = true;
    }

    Card *source;
    int minCards;
    int maxCards;
    bool done;
};

class EventReorderDeck : public Event
{
public:
    EventReorderDeck(Card *_source, UINT _player, int _cardCount)
    {
        done = false;
        source = _source;
        player = _player;
        cardCount = _cardCount;
    }
    bool CanProcessDecisions() const
    {
        return true;
    }
    bool Advance(State &s)
    {
        if(done) return true;

        PlayerState &p = s.players[player];
        s.decision.SelectCards(source, 1, 1);
        s.decision.controllingPlayer = player;
        if(decisionText)
        {
            if(cardCount == 2) s.decision.text = "Choose a card to put 2nd from the top:";
            else if(cardCount == 3) s.decision.text = "Choose a card to put 3rd from the top:";       
            else if(cardCount == 4) s.decision.text = "Choose a card to put 4th from the top:";       
            else if(cardCount == 5) s.decision.text = "Choose a card to put 5th from the top:";
            else s.decision.text = "Choose a card to on your deck:";
        }

        for(int cardIndex = 0; cardIndex < cardCount; cardIndex++)
        {
            s.decision.AddUniqueCard(p.deck[p.deck.Length() - 1 - cardIndex]);
        }

        return false;
    }
    void ProcessDecision(State &s, const DecisionResponse &response)
    {
        if(logging) s.LogIndent(1, player, "puts " + response.cards[0]->PrettyName() + " on top of their deck");
        PlayerState &p = s.players[player];
        for(int cardIndex = 0; cardIndex < cardCount; cardIndex++)
        {
            if(p.deck[p.deck.Length() - 1 - cardIndex] == response.cards[0])
            {
                Utility::Swap(p.deck[p.deck.Length() - cardCount], p.deck[p.deck.Length() - 1 - cardIndex]);
                if(logging && cardCount == 2) s.LogIndent(1, player, "puts " + p.deck.Last()->PrettyName() + " on top of their deck");
                done = true;
                return;
            }
        }
        SignalError("Response card not found");
    }

    Card *source;
    UINT player;
    int cardCount;
    bool done;
};

class EventChooseCardToGain : public Event
{
public:
    EventChooseCardToGain(Card *_source, UINT _controllingPlayer, UINT _gainingPlayer, bool _optionalGain, int _minCost, int _maxCost, CardFilter _filter, GainZone _zone)
    {
        source = _source;
        controllingPlayer = _controllingPlayer;
        gainingPlayer = _gainingPlayer;
        minCost = _minCost;
        maxCost = _maxCost;
        optionalGain = _optionalGain;
        zone = _zone;
        filter = _filter;
        done = false;
    }
    bool CanProcessDecisions() const
    {
        return true;
    }
    bool Advance(State &s)
    {
        if(done) return true;

        s.decision.GainCardFromSupply(s, source, minCost, maxCost, filter);
        if(optionalGain) s.decision.minimumCards = 0;
        s.decision.controllingPlayer = controllingPlayer;

        if(s.decision.cardChoices.Length() == 0)
        {
            if(logging) s.LogIndent(1, gainingPlayer, "has no cards to gain");
            s.decision.type = DecisionNone;
            return true;
        }

        return false;
    }
    void ProcessDecision(State &s, const DecisionResponse &response)
    {
        if(response.cards.Length() > 0)
        {
            s.stack.PushEnd(new EventGainCard(gainingPlayer, response.cards[0], false, false, zone));
        }
        else
        {
            if(logging) s.LogIndent(1, gainingPlayer, "does not gain a card");
        }
        done = true;
    }

    Card *source;
    UINT controllingPlayer, gainingPlayer;
    int minCost, maxCost;
    GainZone zone;
    CardFilter filter;
    bool optionalGain;
    bool done;
};

*/