#ifndef IMPERIUM_GAME_EVENTS_BASE_HPP
#define IMPERIUM_GAME_EVENTS_BASE_HPP

#include <imperium/cards/base_card.hpp>
#include <imperium/game/event_interface.hpp>

namespace imperium
{
namespace game
{
  struct state;
namespace events
{

class remodel_expand : public event_interface
{
  public:
    using card_ptr = std::shared_ptr<cards::base_card>;
    remodel_expand(card_ptr c, unsigned short gain);

    bool can_process_decisions() const override;
    void process_decision(state& s, const decision_response& response) override;
    bool advance(state &s) override;

    card_ptr source;
    unsigned short gained_value;
    card_ptr trashed_card;
    bool done;
};

} // events
} // game
} // imperium
#endif // IMPERIUM_GAME_EVENTS_BASE_HPP