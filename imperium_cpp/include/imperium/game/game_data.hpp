#ifndef IMPERIUM_GAME_GAME_DATA_HPP
#define IMPERIUM_GAME_GAME_DATA_HPP

#include <imperium/cards/card_database.hpp>
#include <imperium/game/player.hpp>

#include <string>
#include <memory>
#include <vector>

namespace imperium
{
namespace game
{
  struct game_options
  {
    using card_ptr = std::shared_ptr<cards::base_card>;
    using card_vec = std::vector<card_ptr>;

    game_options()
    {
        // startingCondition = StartingCondition34Split;
        // prosperity = false;
    }
    // Card* RandomSupplyCard(const CardDatabase &cards) const;
    // void SetupGame(const CardDatabase &cards, const String &options);
    // void RandomizeSupplyPiles(const CardDatabase &cards);
    // void RandomizeSupplyPiles(const CardDatabase &cards, const Vector<String> &requiredCards);
    // String ToString() const;

    card_vec supplyPiles = card_vec();
    // StartingCondition startingCondition;
    bool prosperity = false;
  };


  // BuyLedger is a record of all voluntary purchases made by each player, used by TestChamber
  struct buy_ledger
  {
    using card_ptr = std::shared_ptr<cards::base_card>;
    using card_vec = std::vector<card_ptr>;

    void reset();
    void record_buy(card_ptr c);

    card_vec cards_bought = card_vec();
  };


  struct player_info
  {
    using player_ptr = std::shared_ptr<player>;
    player_info(unsigned int idx, const std::string& n, player* c);
    player_info();

    unsigned int index      = 0;
    std::string  name       = std::string();
    player_ptr   controller = player_ptr(nullptr);

    mutable buy_ledger ledger = buy_ledger();
  };

  struct base_cards_struct
  {
    using card_ptr = std::shared_ptr<cards::base_card>;
    card_ptr copper     = card_ptr(nullptr);
    card_ptr silver     = card_ptr(nullptr);
    card_ptr gold       = card_ptr(nullptr);
    card_ptr platinum   = card_ptr(nullptr);
    card_ptr potion     = card_ptr(nullptr);
    card_ptr estate     = card_ptr(nullptr);
    card_ptr duchy      = card_ptr(nullptr);
    card_ptr province   = card_ptr(nullptr);
    card_ptr colony     = card_ptr(nullptr);
    card_ptr curse      = card_ptr(nullptr);

    // card_ptr moat;
    card_ptr bridge            = card_ptr(nullptr);
    // card_ptr coppersmith;
    card_ptr quarry            = card_ptr(nullptr);
    // card_ptr talisman;
    // card_ptr hoard;
    // card_ptr goons;
    // card_ptr grandMarket;
    card_ptr peddler           = card_ptr(nullptr);
    // card_ptr bank;
    card_ptr royal_seal        = card_ptr(nullptr);
    card_ptr watchtower        = card_ptr(nullptr);
    // card_ptr lighthouse;
    // card_ptr treasury;
    // card_ptr treasureMap;
    // card_ptr tradeRoute;
    // card_ptr mint;
    // card_ptr champion;
    // card_ptr gardener;
    // card_ptr grandCourt;
    card_ptr plunder            = card_ptr(nullptr);
    // card_ptr promisedLand;
    // card_ptr trailblazer;
    // card_ptr evangelist;
    // card_ptr furnace;
    // card_ptr witchdoctor;
  };


  struct game_data
  {
    using info_vec = std::vector<player_info>;
    using card_ptr = std::shared_ptr<cards::base_card>;
    using card_vec = std::vector<card_ptr>;

    static const unsigned int npos = -1;

    void new_game(const info_vec& playerList, const game_options& opts);
    void init(const cards::card_database& db);

    const cards::card_database* cards       = nullptr;
    base_cards_struct           base_cards;
    game_options                options;

    info_vec players = info_vec();
    card_vec supply_cards = card_vec();

    // Fast "in-supply" checks for certain cards
    bool use_gain_list          = false;
    bool quarry_in_supply       = false;
    bool coppersmith_in_supply  = false;
    bool bridge_in_supply       = false;
    bool talisman_in_supply     = false;
    bool hoard_in_supply        = false;
    bool goons_in_supply        = false;
    bool grandMarket_in_supply  = false;
    bool treasury_in_supply     = false;
    bool royal_seal_in_supply    = false;
    bool watchtower_in_supply   = false;
    bool trade_route_in_supply  = false;
    bool mint_in_supply         = false;
    bool champion_in_supply     = false;
    bool gardener_in_supply     = false;
    bool grand_court_in_supply  = false;
    bool plunder_in_supply      = false;
    bool promisedLand_in_supply = false;
    bool furnace_in_supply      = false;

    inline unsigned int supply_index(card_ptr c) const
    {
      for( unsigned int i = 0; i < supply_cards.size(); ++i ) { if(supply_cards[i] == c) return i; }
      return npos;
    }

    game_data(const game_data&)             = delete;
    game_data& operator=(const game_data&)  = delete;

    private:
      void init_base_cards();
  };

} // game
} // imperium

#endif // IMPERIUM_GAME_GAME_DATA_HPP




/*


struct GameData;
class Player;



class Log
{
public:
    void operator()(const String &s)
    {
        _events.PushEnd(s);
    }
    void Reset()
    {
        _events.FreeMemory();
    }
    const Vector<String>& Events()
    {
        return _events;
    }

private:
    Vector<String> _events;
};


struct GameData
{
    void NewGame(const Vector<PlayerInfo> &playerList, const GameOptions &_options);
    


    mutable Log log;


    Vector<Card*> costModifyingCards;
    Vector<Card*> reactionCards;




};


*/