#ifndef IMPERIUM_GAME_EVENT_INTERFACE_HPP
#define IMPERIUM_GAME_EVENT_INTERFACE_HPP

namespace imperium
{
namespace game
{
  class state;
  class decision_response;


  enum class gain_zone
  {
    to_discard,
    to_discard_ironworks,
    to_hand,
    to_deck_top,
    to_trash,
  };

  // attack_annotations is special annotation that attack events must store. They indicate which reactions
  // have already been processed for the attack.
  struct attack_annotations
  {
    attack_annotations() {}
    bool moat_processed = false;
  };

  // A list of Event objects composes the event stack of a Dominion game.
  // If Advance returns true, the event is done and should be removed, otherwise it will keep recurring.
  class event_interface
  {
    public:
      static const unsigned int npos = -1;

      virtual ~event_interface();
      virtual bool advance(state& s) = 0;
      virtual bool is_attack() const;
      virtual unsigned int attacked_player() const;
      virtual attack_annotations* annotations();
      virtual bool can_process_decisions() const;
      virtual bool destroy_next_event_on_stack() const;
      virtual void process_decision(state& s, const decision_response& response);
  };
} // game
} // imperium
#endif // IMPERIUM_GAME_EVENT_INTERFACE_HPP

/*



*/