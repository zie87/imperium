#ifndef IMPERIUM_CARDS_BASE_CARD_HPP
#define IMPERIUM_CARDS_BASE_CARD_HPP

#include <imperium/cards/card_effect_interface.hpp>

#include <vector>
#include <memory>
#include <iostream>
#include <string>

namespace imperium
{
namespace cards
{
  struct base_card_properties
  {
    using type_list = std::vector<std::string>;

    base_card_properties(const std::string& n, const std::string& e)
    : name(n), expansion(e)
    {}

    std::string name      = std::string();
    std::string expansion = std::string();

    unsigned short cost     = 0;
    unsigned short pcost    = 0;
    unsigned short supply   = 0;  
    type_list      types    = type_list();
    unsigned short actions  = 0;
    unsigned short cards    = 0; 
    unsigned short buys     = 0; 
    unsigned short money    = 0;
    unsigned short v_tokens = 0;
    
    short v_points = 0;
    short treasure = 0;
  };

  std::ostream& operator<<(std::ostream& os, const base_card_properties& p);

  class base_card
  {
    public:
      using effect_ptr = std::shared_ptr<card_effect_interface>;

      base_card();
      base_card(const base_card_properties& props);
      virtual ~base_card();

      // basic properties
      std::string name      = std::string();
      std::string expansion = std::string();

      unsigned short costs          = 0;
      unsigned short portion_costs  = 0;
      unsigned short supply         = 0;

      // actions
      unsigned short actions  = 0;
      unsigned short cards    = 0;
      unsigned short buys     = 0;
      unsigned short money    = 0;
      
      short victory_tokens = 0;

      // victory or curse
      short victory_points = 0;
      // treasure
      short treasure = 0;

      // properties functions for types
      void set_properties(const base_card_properties& properties) noexcept;

      bool is_victory()   const noexcept;
      bool is_treasure()  const noexcept;
      bool is_action()    const noexcept;
      bool is_reaction()  const noexcept;
      bool is_duration()  const noexcept;
      bool is_attack()    const noexcept;
      bool is_permanent() const noexcept;

      void set_victory(bool is)   noexcept;
      void set_treasure(bool is)  noexcept;
      void set_action(bool is)    noexcept;
      void set_reaction(bool is)  noexcept;
      void set_duration(bool is)  noexcept;
      void set_attack(bool is)    noexcept;
      void set_permanent(bool is) noexcept;

      effect_ptr effect() const noexcept;
      void effect(card_effect_interface* e) noexcept;
      void effect(effect_ptr e) noexcept;

      // ai informations
      int priority = 0;
      int strength = 0;

      // utility function
      bool is_pure_victory() const noexcept;

    protected:
      // type informations maybe chang this to enums and calculation
      bool m_is_victory   = false;
      bool m_is_treasure  = false;
      bool m_is_action    = false;
      bool m_is_reaction  = false;
      bool m_is_duration  = false;
      bool m_is_attack    = false;
      bool m_is_permanent = false;

      effect_ptr m_card_effect = effect_ptr(nullptr);

  };
} // cards
} // imperium

#endif// IMPERIUM_CARDS_BASE_CARD_HPP

  