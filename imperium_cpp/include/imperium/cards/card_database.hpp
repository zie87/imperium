#ifndef IMPERIUM_CARDS_CARD_DATABASE_HPP
#define IMPERIUM_CARDS_CARD_DATABASE_HPP

#include <imperium/cards/base_card.hpp>

#include <map>
#include <vector>

namespace imperium
{
namespace cards
{
  class card_database
  {
    public:
      using card_ptr        = std::shared_ptr<base_card>;
      using effect_ptr        = std::shared_ptr<card_effect_interface>;

      using name_list       = std::vector<std::string>;
      using card_list       = std::vector<card_ptr>;
      using card_map        = std::map<std::string, card_ptr>;
      using card_map_pair   = std::pair<std::string, card_ptr>;
      using properties_map  = std::map<std::string, base_card_properties>;

      card_database();
      ~card_database();

      void init(bool use_custom_cards = false);

      card_ptr get_card(const std::string& name) const;
      // base_card* random_supply_card() const;

      name_list bypassed_cards() const noexcept { return m_skip_list; }

    private:
      void register_card_effects(bool useCustomCards);
      void register_cards( const std::string& card_name, effect_ptr effect );
      void load_expansion_properties(const std::string& filename, const std::string& expansion);

      name_list m_skip_list;
      card_list m_card_list;
      card_map  m_cards;
  };


} // cards
} // imperium

#endif // IMPERIUM_CARDS_CARD_DATABASE_HPP


// class CardDatabase
// {
// public:
//     void Init(bool useCustomCards);

//     Card* GetCard(const String &s) const;
//     Card* RandomSupplyCard() const;

// private:
//     void RegisterCard(const String &cardName, CardEffect *effect);
//     void LoadCardList(const String &filename, const String &expansion);
//     bool IsSupplyCard(const Card *c) const;
//     void LoadPriorityList();
//     void LoadStrengthList();

//     map<String, Card*> _cards;
//     Vector<Card*> _cardList;
//     Vector<String> _skipList;
// };
