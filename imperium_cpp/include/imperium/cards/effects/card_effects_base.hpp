#ifndef IMPERIUM_CARDS_CARD_EFFECTS_BASE_HPP
#define IMPERIUM_CARDS_CARD_EFFECTS_BASE_HPP

#include <imperium/cards/card_effect_interface.hpp>

namespace imperium
{
namespace cards
{
namespace effects
{

  class base_chapel : public card_effect_interface
  {
    public:
      void play_action(game::state& s) const override;
      bool can_process_decisions() const override;
      void process_decision(game::state& s, const game::decision_response& response) const override;
  };

  class base_cellar : public card_effect_interface
  {
    public:
      void play_action(game::state& s) const override;
      bool can_process_decisions() const override;
      void process_decision(game::state& s, const game::decision_response& response) const override;
  };

  class base_moneylender : public card_effect_interface
  {
    public:
      void play_action(game::state& s) const override;
  };

  class base_workshop : public card_effect_interface
  {
    public:
      void play_action(game::state& s) const override;
      bool can_process_decisions() const override;
      void process_decision(game::state& s, const game::decision_response& response) const override;
  };

  class base_feast : public card_effect_interface
  {
    public:
      void play_action(game::state& s) const override;
      bool can_process_decisions() const override;
      void process_decision(game::state& s, const game::decision_response& response) const override;
  };

  class base_chancellor : public card_effect_interface
  {
    public:
      void play_action(game::state& s) const override;
      bool can_process_decisions() const override;
      void process_decision(game::state& s, const game::decision_response& response) const override;
  };

  class base_militia : public card_effect_interface
  {
    public:
      void play_action(game::state& s) const override;
  };

  class base_council_room : public card_effect_interface
  {
    public:
      void play_action(game::state& s) const override;
  };

  class base_remodel : public card_effect_interface
  {
    public:
      void play_action(game::state& s) const override;
  };

} // effects
} // cards
} // imperium

#endif // IMPERIUM_CARDS_CARD_EFFECTS_BASE_HPP