#ifndef IMPERIUM_CARDS_CARD_EFFECT_INTERFACE_HPP
#define IMPERIUM_CARDS_CARD_EFFECT_INTERFACE_HPP

#include <memory>

namespace imperium
{
  namespace game
  {
    struct state;
    struct decision_response;
  } // game

namespace cards
{
  class base_card;

  class card_effect_interface
  {
    public:
      using card_ptr = std::shared_ptr<cards::base_card>;

      virtual ~card_effect_interface();

      virtual void play_action(game::state& s) const;
      virtual bool can_process_decisions() const;

      virtual void process_decision(game::state& s, const game::decision_response& response) const;
      
      virtual void process_duration(game::state& s) const;
      virtual unsigned short victory_points(const game::state& s, unsigned int player_idx) const;
  
      card_ptr card = card_ptr(nullptr);
  };

} // cards
} // imperium

#endif// IMPERIUM_CARDS_CARD_EFFECT_INTERFACE_HPP
