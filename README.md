# Imperium

This is a fork of fork of [regnancy](https://bitbucket.org/BigYellowCactus/regnancy/)

This game is a python implementation of the board game Dominion. For more information about Dominion, visit [BoardgameGeek][1]

### Imperium in action

![Imperium in action][2]

### The deckeditor

![The deckeditor][3]

## Running the game

In order to run Imperium, you have to follow the following steps

### Install Python

In order to build the game, you need Python 2.7

Either download Python from its [website][4], or use your package manager to get it.

Imperium works fine with Python 2.7 (it won't run on Python 3).

### Install Pygame

Visit [pygame.org][5] or use your package manager to get it.

### Get the source!

Get the Imperium source by cloning the Imperium repository

`$ git clone git@bitbucket.org:zie87/imperium.git`

### Run the game

You'll find a file named `regnancy.py` in the `imperium_py`-folder. Running this file starts Imperium.

## Images

Imperium does not contain the original card images. But when you run the game, Imperium will try to download them from various websites. Don't get shocked if you see a lot of red squares labeled `ERR` when you first run Imperium. Just be patient while Imperium downloads the images.

[1]: http://boardgamegeek.com/boardgame/36218/dominion
[2]: https://bitbucket.org/BigYellowCactus/regnancy/downloads/reg1.jpg
[3]: https://bitbucket.org/BigYellowCactus/regnancy/downloads/reg2.jpg
[4]: http://python.org/
[5]: http://pygame.org

## Useful Links

* [regnancy](https://bitbucket.org/BigYellowCactus/regnancy/)
* [provincal](http://graphics.stanford.edu/~mdfisher/DominionAI.html)
* [provincal source code](https://github.com/techmatt/Provincial)
